// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "ue4proj.h"
#include "ue4projGameMode.h"
#include "ue4projPlayerController.h"
#include "ue4projCharacter.h"

Aue4projGameMode::Aue4projGameMode(const class FPostConstructInitializeProperties& PCIP) : Super(PCIP)
{
	// use our custom PlayerController class
	PlayerControllerClass = Aue4projPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/MyCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}